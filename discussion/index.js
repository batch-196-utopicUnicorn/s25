

//JSON
//JSON or  Javascript Object Notation is a popular data format for application to communicate with each other.
//JSON may look like a Javascript Object but it is actually a string.
/*

	JSON syntax

	{
		"key1": "valueA",
		"key2": "valueB"
	
	}

	Keys are wrapped in curly brace

*/

let sample1 =`

	{
		"name": "Katniss Everdeen",
		"age": 20,
		"address": {
			"city": "Kansas City",
			"state": "Kansas"
		}
	}`;

console.log(sample1)

//Are we able to turn a JSON into a JS object?
//JSON.parse() - will return the JSON as JS Object

console.log(JSON.parse(sample1));

//JSON Array
//JSON array is an array of JSON

let sampleArr = `
	[
		{
			"email": "jasonNewsted@gmail.com",
			"password": "iplaybass1234",
			"isAdmin": false
		},
		{
			"email": "larsDrums@gmail.com",
			"password": "metallicaMe80",
			"isAdmin": true
		}
	]



`;
console.log(sampleArr);
//Can we use Array Methods on a JSON array?
//No. Because a JSON is a string.
//So what can we do to be able to add more items/object into our sampleArr?
//Parsed the JSON array to a JS array and saved it in a variable;
let parsedSampleArr = JSON.parse(sampleArr);
console.log(parsedSampleArr);

//Can we not delete the last item in the JSON Array?
console.log(parsedSampleArr.pop());
console.log(parsedSampleArr);

//If for example we need to send this data back to the client/front end, it should be in JSON format

//JSON.parse() does not mutate or update the original JSON
//We can actually turn a JS object into a JSON
//JSON.stringify() - will stringify JS objects as JSON

sampleArr = JSON.stringify(parsedSampleArr)
console.log(sampleArr);

//Database => Server/API (JSON to JS object to process) => sent as JSON => client

/*
	given a JSON array, process it and convert to a JS object so we can manipulate the array.

	Delete the last item in the array and add a new item in the array.

	Stringify the array back in JSON

*/

let jsonArr = `

	[
		"pizza",
		"hamburger",
		"spaghetti",
		"shanghai",
		"hotdog stick on a pineapple",
		"pancit bihon"
	]


`;

let newJsonArr = JSON.parse(jsonArr);
newJsonArr.pop();
newJsonArr.push("talaba");
newJsonArr = JSON.stringify(newJsonArr);
console.log(newJsonArr)